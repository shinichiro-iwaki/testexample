# TestExample

[豆蔵デベロッパーサイト](https://developer.mamezou-tech.com/)のブログ記事で利用しているサンプルコード  

## サンプルアプリについて

### 概要  
サンプルコードでよくある「Hello World」を、フロントエンド/バックエンドに分割して実現しました。  
実装にあたり、以下の技術を利用しています。  

| stack | version | note | 
| ---- | ---- | ---- | 
| Spring boot | 2.7.5 | アプリケーションフレームワーク | 
| React | 18.2.0 | フロントエンド向けアプリケーションフレームワーク | 
| Next.js | 13.0.6 | フロントエンド向けアプリケーションフレームワーク | 
| OpenAPI generator | 6.2.1 | フロント/バック間のAPI定義 | 

### ビルドと実行  
サンプルアプリのビルドには以下の環境が必要です。  
* バックエンド:Java11(※)とGradle  
* フロントエンド:node18(※)とyarnまたはnpm  

 ※他Versionについては動作するだろうとは思いますが現状未検証です  

1. アプリケーションのビルド準備  
 ```
 # Clone this repository
 git clone https://gitlab.com/shinichiro-iwaki/testexample.git
 # Go into the repository
 cd testexample
 ```
1. バックエンドのビルドと実行  
 ```
 # Go into the app project
 cd back
 # Build applications
 gradlew build
 # Run applications
 java -jar build/libs/greet-server-1.2.0.jar
 ```
1. フロントエンドの実行  
 ```
 # Go into the app project
 cd front
 # Build applications
 yarn build
 # Run applications
 yarn start
 ```

### 動作確認  
フロントエンドのURL(`http://localhost:3000`)にブラウザでアクセスすると、画面にブラウザのロケールに応じた(※)あいさつ文が表示されます。  
フロントエンドはNext.jsを利用して実装しているため、create-next-appの初期画面にあいさつ文を追加したものになっています。  
※:といっても、日本語/英語だけですが  
![image](img/app-screen.jpg)

## 参照記事

以下の記事で参照しています。

| 記事 | branch | tags | 
| ---- | ---- | ---- | 
| [Contract TestツールPactの紹介](https://developer.mamezou-tech.com/blogs/2022/12/03/contract-test-with-pact/) | feature-api-integration | - Pact-contract-test-local <br> - Pact-contract-test-broker | 
| [Contract Testの使いどころを考える](https://developer.mamezou-tech.com/blogs/2022/12/09/contract-test-usecase/) | feature-api-integration | - Pact-contract-mistatch <br> - Pact-contract-revise | 
| [Contract Testを異言語間で実施する](https://developer.mamezou-tech.com/blogs/2023/03/09/contract-test-multilang/) | feature-ts-front | - Pact-ts-local | 
| [Contract TestをGitlab CIのパイプラインに組み込む](https://developer.mamezou-tech.com/blogs/2023/04/03/contract-test-pipeline/) | feature-ts-front | - Pact-pipeline | 
| [テストのフレイキーさを簡単に確認するためにレポートツールAllureを利用する](https://developer.mamezou-tech.com/blogs/2023/05/11/flaky-test-allure/) | fature-allure-reporting | - Allure-flaky-tests <br> - Allure-fix-flakieness | 
| [ミューテーションテストの技法でテストの十分性を評価する](https://developer.mamezou-tech.com/blogs/2024/12/03/mutation-testing/) | (feature-mutation-test |  | 
| Now Printing |  |  | 
  
