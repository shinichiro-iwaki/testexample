import React, {useState, useEffect} from 'react'
import {useLocale} from 'react-aria'
import { GreetApi, GreetMessage } from '@shinichiro-iwaki/greeter-api/src/client/greeter'

const Greet = () => {
    const [greet, setGreet] = useState<GreetMessage>()
    let locale = useLocale()

    useEffect(() => {
        const greetApi = new GreetApi(undefined, process.env.NEXT_PUBLIC_API_URL);
        greetApi.getGreetIn(locale.locale.slice(0,2), {
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
        })
        .then(response => {
            setGreet(response.data)   
       })
    },[])

    if (!greet) return null;

    return (
        <div>
            <li>{greet.content}</li>
        </div>
    )
}

export default Greet
