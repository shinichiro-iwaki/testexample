import { pactWith } from 'jest-pact/dist/v3';
import { MatchersV3 } from '@pact-foundation/pact';
import { Configuration, GreetApi } from '@shinichiro-iwaki/greeter-api/src/client/greeter'

pactWith({ consumer: 'Greet_Front', provider: 'GreetProvider' }, (interaction) => {
  interaction('A request for API greet', ({ provider, execute }) => {
    beforeEach(() =>
      provider
        .uponReceiving('A request for API greet')
        .withRequest({
          method: 'GET',
          path: '/greet/en',
        })
        .willRespondWith({
          status: 200,
          body: {
            id: 1,
            content: MatchersV3.like("Hello Microservice")
          },
        })
    );

    execute('greet api call', (mockserver) =>
      new GreetApi(new Configuration({ basePath: mockserver.url, }))
        .getGreetIn('en')
        .then((response) => {
          expect(response.data.id).toEqual(1);
          expect(response.data.content).toEqual("Hello Microservice");
        })
    );
  });
});
