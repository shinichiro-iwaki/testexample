package com.example.iwaki.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest()
public class GreetServiceTest {
	private final GreetService target;

    @Autowired
	public GreetServiceTest(GreetService greetService) {
        this.target = greetService;
    }

    @MockBean
	Clock clock;

	@Test
	@DisplayName("通常のあいさつ")
	void testDefaultGreet() {
		when(clock.instant()).thenReturn(Instant.parse("2023-04-06T03:00:00Z"));
		when(clock.getZone()).thenReturn(ZoneId.of("Asia/Tokyo"));
	    String expectedEnglish = "Hello Microservice";
		String actual = target.greetIn("en");

        assertEquals(expectedEnglish, actual);
	}
}
