package com.example.iwaki.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import java.math.BigDecimal;

import com.example.iwaki.service.GreetService;
import com.example.iwaki.greet.api.GreetApi;
import com.example.iwaki.greet.model.GreetMessage;

@RestController
@CrossOrigin
public class GreetController implements GreetApi {

    private final GreetService greeter;
    
    @Autowired
    public GreetController(GreetService greeter) {
        this.greeter = greeter;
    }

	@Override
	public ResponseEntity<GreetMessage> getGreet() {
		GreetMessage target = new GreetMessage();
		target.setId(BigDecimal.ONE);
		target.setContent(greeter.greetIn("en"));
		return ResponseEntity.ok(target);
	}

	@Override
	public ResponseEntity<GreetMessage> getGreetIn(String lang) {
		GreetMessage target = new GreetMessage();
		target.setId(BigDecimal.ONE);
		target.setContent(greeter.greetIn(lang));
		return ResponseEntity.ok(target);
	}
}