package com.example.iwaki.service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class GreetService {

    private final Clock clock;

    @Autowired
    public GreetService(Clock clock) {
		this.clock = clock;
	}

	@Autowired
	MessageSource messages;

    public String greetIn(String lang) {
        int hour = LocalDateTime.now(clock).getHour();
        String greetId;
        if (4 <= hour && hour < 11) {
            greetId = "greet.morning";
        } else if (11 <= hour && hour < 18) {
            greetId = "greet.default";
        } else {
            greetId = "greet.night";
        }
        return messages.getMessage(greetId, new String[]{}, new Locale(lang));
    }
}